## Installing

You need to have `virtualenv` and Python 3 installed (Django 2 requires Python 3) then:

First create a new virtual environment and activate it with:

```bash
 virtualenv -p python3 env
 source env/bin/activate
```

Next, clone the project:

```bash
git clone https://deltahat@bitbucket.org/deltahat/api_rest.git
cd api_rest/django-auth0-vue
```

Install the project requirements using pip:

```bash
pip install -r requirements.txt
```

Install the Vue.js dependencies and run the front-end dev server with:

```bash
cd vueapp
npm install
npm run dev
```

Migrate the database then run the Django dev server with:

```bash
python manage.py migrate
python manage.py runserver
```
Execute the calculator of rate:

```bash
cd api_rest/django-auth0-vue/catalog
python dollarate.py
```

You can now navigate with your web browser to http://localhost:8000 and start playing with the demo.

[![N|Solid](https://image.ibb.co/gpzjBJ/Captura_de_pantalla_2018_05_15_18_04_43.png)](https://bitbucket.org/deltahat/api_rest/src/master/django-auth0-vue/)
