from django.contrib import admin
from django.urls import path

from django.conf.urls import url
from django.views.generic import TemplateView
from django.views.generic import RedirectView
from catalog import views

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
    url(r'^api/campaings/$', views.campaing_list),
    url(r'^api/campaings/(?P<pk>[0-9]+)$', views.campaing_detail),
    path('admin/', admin.site.urls),
    url(r'^(?:.*)/?$', TemplateView.as_view(template_name='index.html'), name='catchall'),

]
