from django.db import migrations
from django.conf import settings

def create_data(apps, schema_editor):
    Campaing = apps.get_model('catalog', 'Campaing')
    User = apps.get_model(settings.AUTH_USER_MODEL)

    user = User(pk=1, username="auth0user", is_active=True , email="giuliano.brunero@gmail.com")
    user.save()

    Campaing(name='Foo', cost=1, revenue=1.8 , currency='ARS', conversions=48).save()
    Campaing(name='Aoo', cost=1, revenue=1.8 , currency='ARS', conversions=48).save()
    Campaing(name='Doo', cost=1, revenue=1.8 , currency='ARS', conversions=48).save()
    Campaing(name='Xoo', cost=1, revenue=1.8 , currency='ARS', conversions=48).save()
    Campaing(name='Foo', cost=1, revenue=1.8 , currency='ARS', conversions=48).save()
    Campaing(name='Foo', cost=1, revenue=1.8 , currency='ARS', conversions=48).save()
    Campaing(name='Eoo', cost=1, revenue=1.8 , currency='ARS', conversions=48).save()
    Campaing(name='Foo', cost=1, revenue=1.8 , currency='ARS', conversions=48).save()
    Campaing(name='Loo', cost=1, revenue=1.8 , currency='ARS', conversions=48).save()
    Campaing(name='Foo', cost=1, revenue=1.8 , currency='ARS', conversions=48).save()


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_data),
    ]
