from rest_framework import serializers
from .models import Campaing

class CampaingSerializer(serializers.ModelSerializer):
    total = serializers.ReadOnlyField(source='get_total')
    
    class Meta:
        model = Campaing
        fields = ('pk', 'name', 'cost', 'revenue', 'currency', 'conversions', 'total')
