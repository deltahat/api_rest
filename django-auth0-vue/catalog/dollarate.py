# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
import time

# Extraction of source code
url = 'http://www.bna.com.ar/'
r = requests.get(url)

# Analisis de codigo fuente
soup = BeautifulSoup(r.text, "lxml")

# Extraction of table
html_table = soup.select("table.cotizacion tr")


# Extraction of rate dollar
def get_rate():
    i = 0
    for table_row in html_table:
        cells = table_row.findAll('td')
        if len(cells) > 0:
            if i < 1:
                value = cells[1].text.strip()
                i = i + 1
    value = value.encode("utf-8")
    value = value.replace(",", ".")
    value = float(value)
    value = int(value)
    value = str(value)
    print("Rate dollar: %s" %value)
    return value


# Save data and loop every 30'
while True:
    value = get_rate()
    f = open ('rate','w')
    f.write(value)
    f.close()
    print(". . .")
    time.sleep(1800)
