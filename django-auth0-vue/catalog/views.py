# -*- coding: utf-8 -*-

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Campaing
from .serializers import *

@csrf_exempt
@api_view(['GET', 'POST'])
def campaing_list(request):
    """
    List  campaings, or create a new campaing.
    """
    if request.method == 'GET':
        data = []
        nextPage = 1
        previousPage = 1
        campaings = Campaing.objects.all()
        page = request.GET.get('page', 1)
        paginator = Paginator(campaings, 10)
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        serializer = CampaingSerializer(data,context={'request': request} ,many=True)
        if data.has_next():
            nextPage = data.next_page_number()
        if data.has_previous():
            previousPage = data.previous_page_number()

        return Response({'data': serializer.data , 'count': paginator.count, 'numpages' : paginator.num_pages, 'nextlink': '/api/campaings/?page=' + str(nextPage), 'prevlink': '/api/campaings/?page=' + str(previousPage)})

    elif request.method == 'POST':
        serializer = CampaingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def campaing_detail(request, pk):
    """
    Retrieve, update or delete a campaing instance.
    """
    try:
        campaing = Campaing.objects.get(pk=pk)
    except Campaing.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = CampaingSerializer(campaing,context={'request': request})
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = CampaingSerializer(campaing, data=request.data,context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        campaing.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
