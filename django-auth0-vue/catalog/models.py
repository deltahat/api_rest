from django.db import models

class Campaing(models.Model):

    name = models.CharField(max_length=200, help_text="Enter campaing name")
    cost = models.DecimalField(decimal_places=2, max_digits=20,help_text="Enter cost")
    revenue = models.DecimalField(decimal_places=2, max_digits=20,help_text="Enter revenue")
    currency = models.CharField(max_length=200, help_text="Enter currency")
    conversions = models.DecimalField(decimal_places=2, max_digits=20,help_text="Enter conversions")


    def get_total(self):
        f = open ('catalog/rate','r')
        rate = int(f.read())
        f.close()
        # Return benefit USD
        return ((self.revenue - self.cost) * self.conversions) / rate


    def get_absolute_url(self):
         """
         Returns the url to access a particular instance of Campaing.
         """
         return reverse('campaing-detail-view', args=[str(self.id)])


    def __str__(self):
        return self.name
