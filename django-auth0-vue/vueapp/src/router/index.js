
/* eslint-disable */

import Vue from 'vue'
import Router from 'vue-router'
import CampaingList from '@/components/CampaingList'
import CampaingCreate from '@/components/CampaingCreate'
import Callback from '@/components/Callback'
import Home from '@/components/Home'
import AuthService from '../auth/AuthService'

Vue.use(Router)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/campaing-list',
    name: 'CampaingList',
    component: CampaingList,
    meta: { requiresAuth : true }
  },
  {
    path: '/campaing-create',
    name: 'CampaingCreate',
    component: CampaingCreate,
    meta: { requiresAuth : true }
  },
  {
    path: '/campaing-update/:pk',
    name: 'CampaingUpdate',
    component: CampaingCreate
  },
  {
    path: '/callback',
    name: 'Callback',
    component: Callback
  }]

const router = new Router({
  mode: 'history',
  routes
})

//const auth = new AuthService()

router.beforeEach((to, from, next) => {
  console.log('routing ', from, AuthService.authenticated())
  if(to.meta.requiresAuth)
  {
    if(!AuthService.authenticated())
    {
      next('/');
    }
  }
  next()
})

export function authGuard(to, from, next) {

  if(!AuthService.authenticated()){
    next('/');
  }
  next()

}

export default router
