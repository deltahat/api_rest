/* eslint-disable */
import axios from 'axios';
import AuthService from '../auth/AuthService';
const API_URL = 'http://localhost:8000';

export class APIService{
    constructor(){

    }
    getCampaings() {
        const url = `${API_URL}/api/campaings/`;
        return axios.get(url, { headers: { Authorization: `Bearer ${AuthService.getAuthToken()}` }}).then(response => response.data);
    }
    getCampaingsByURL(link){
        const url = `${API_URL}${link}`;
        return axios.get(url, { headers: { Authorization: `Bearer ${AuthService.getAuthToken()}` }}).then(response => response.data);

    }
    getCampaing(pk) {
        const url = `${API_URL}/api/campaings/${pk}`;
        return axios.get(url, { headers: { Authorization: `Bearer ${AuthService.getAuthToken()}` }}).then(response => response.data);
    }
    deleteCampaing(campaing){
        const url = `${API_URL}/api/campaings/${campaing.pk}`;
        return axios.delete(url, { headers: { Authorization: `Bearer ${AuthService.getAuthToken()}` }});

    }
    createCampaing(campaing){
        const url = `${API_URL}/api/campaings/`;
        const headers = {Authorization: `Bearer ${AuthService.getAuthToken()}`};
        return axios.post(url,campaing,{headers: headers});
    }
    updateCampaing(campaing){
        const url = `${API_URL}/api/campaings/${campaing.pk}`;
        const headers = {Authorization: `Bearer ${AuthService.getAuthToken()}`};
        return axios.put(url,campaing,{headers: headers});
    }
}
